﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreAndLevelUI : MonoBehaviour
{
    [SerializeField]
    private Text ScoreText; 
    [SerializeField]
    private Text LevelText;
    
    // Start is called before the first frame update
    public void UpdateScoreAndLevelText(int score, int level)
    {
        ScoreText.text = score.ToString();
        LevelText.text = level.ToString();
    }
}
