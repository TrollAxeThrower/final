﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    #region Serialized Fields
    [SerializeField]
    private BoardView m_BoardView;
    [SerializeField]
    private ScoreAndLevelUI m_ScoreAndLevelUI;
    //private ScoreAndLevelUI m_ScoreAndLevelUI;
    [SerializeField]
    private GameObject GameOverObj;

    [SerializeField]
    private KeyCode upKey = KeyCode.W;
    [SerializeField]
    private KeyCode downKey = KeyCode.S;
    [SerializeField]
    private KeyCode leftKey = KeyCode.A;
    [SerializeField]
    private KeyCode rightKey = KeyCode.D;
    [SerializeField]
    private KeyCode rotateKey = KeyCode.Space;
    #endregion Serialized Fields

    #region Private Members
    private float m_TimeBetweenTicks;
    private float m_TimeBetweenTicksPerLevel;

    private int m_Score;
    private int m_Level;
    private int m_RowsCompleted;

    private Board m_Board;
    private GameState m_GameState;
    #endregion Private Members

    public enum GameState
    {
        AllowInput,
        DontAllowInput
    }

    public void StartGame(float i_TimeBetweenTicks, float i_TimeBetweenTicksPerLevel)
    {
        m_Board = m_BoardView.CreateBoard();
        GameOverObj.SetActive(false);
        m_TimeBetweenTicks = i_TimeBetweenTicks;
        m_TimeBetweenTicksPerLevel = i_TimeBetweenTicksPerLevel;
        m_GameState = GameState.AllowInput;
        m_Score = 0;
        m_RowsCompleted = 0;
        m_Level = 1;

        AddShape();
        m_BoardView.UpdateBoardViewAccordingToBoard();
        m_ScoreAndLevelUI.UpdateScoreAndLevelText(m_Score, m_Level);
        StartCoroutine(TimeTick());
    }

    void Update()
    {
        if (m_GameState == GameState.AllowInput)
            getInput();
    }

    public void AddShape()
    {
        m_Board.AddShapeToTopOfShapeMatrixAtPointerPosition();
    }

    bool isShapeLanded = false;

    private IEnumerator TimeTick()
    {
        isShapeLanded = m_Board.TakeTurn();
        if (isShapeLanded)
        {
            m_Board.AddShapeToTopOfShapeMatrixAtPointerPosition();
        }
        m_BoardView.UpdateBoardViewAccordingToBoard();
        yield return new WaitForSeconds(m_TimeBetweenTicks);
        int sumRowCompleted = m_Board.CollapseAllCompletedRowsAndScore();

        if (sumRowCompleted == 1)
            m_Score += Board.ScoreOneRow;
        if (sumRowCompleted == 2)
            m_Score += Board.ScoreTwoRows;
        if (sumRowCompleted == 3)
            m_Score += Board.ScoreThreeRows;
        if (sumRowCompleted == 4)
            m_Score += Board.ScoreFourRows;

        m_RowsCompleted += sumRowCompleted;

        if (sumRowCompleted >= 1)
        {
            m_BoardView.UpdateBoardViewAccordingToBoard();
            m_TimeBetweenTicks *= m_TimeBetweenTicksPerLevel;
            m_ScoreAndLevelUI.UpdateScoreAndLevelText(m_Score, m_Level);
            if (m_RowsCompleted != 0)
                m_Level = 10 / m_RowsCompleted;
        }
        if(m_Board.IsGameOver())
        {
            Debug.Log("Game Over");
            GameOverObj.SetActive(true);
            m_GameState = GameState.DontAllowInput;
        }
        else
            StartCoroutine(TimeTick());
    }

    private void getInput()
    {
        if (Input.GetKeyUp(upKey))
            m_Board.TryMovePointer(1, 0);
        if (Input.GetKeyUp(downKey))
            m_Board.TryMovePointer(-1, 0);
        if (Input.GetKeyUp(leftKey))
            m_Board.TryMovePointer(0, 1);
        if (Input.GetKeyUp(rightKey))
            m_Board.TryMovePointer(0, -1);
        if (Input.GetKeyUp(rotateKey))
            m_Board.RotateShape();

        m_BoardView.UpdateBoardViewAccordingToBoard();
    }
}
