﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TetrisGameManager : MonoBehaviour
{
    public Player Player1;
    public Player Player2;

    //public InputManager Player2Input;

    [SerializeField]
    private float TimeBetweenTicks = 1f;
    [SerializeField]
    private float TimeBetweenTicksPerLevel = 0.8f;

    [SerializeField]
    private bool isTwoPlayer;
    // Start is called before the first frame update
    void Start()
    {
        ActivatePlayer(Player1);

        if (isTwoPlayer)
        {
            ActivatePlayer(Player2);
        }
    }

    private void ActivatePlayer(Player player)
    {
        player.StartGame(TimeBetweenTicks, TimeBetweenTicksPerLevel);
    }
}
