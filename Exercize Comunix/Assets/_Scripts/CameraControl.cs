﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour
{
    [SerializeField]
    private GameObject target;
    [SerializeField]
    private float speed = 10.0f;

    [SerializeField]
    private KeyCode LeftKey;
    [SerializeField]
    private KeyCode RightKey;

    private Vector3 point;
    private bool LeftPressed;
    void Start()
    {
        point = target.transform.position;
        transform.LookAt(point);
    }

    void Update()
    {
        if (Input.GetKey(LeftKey))
            transform.RotateAround
                (point,
                Vector3.up,
                20 * Time.deltaTime * speed);

        if (Input.GetKey(RightKey))
            transform.RotateAround
                (point,
                Vector3.up,
                -20 * Time.deltaTime * speed);
    }
}