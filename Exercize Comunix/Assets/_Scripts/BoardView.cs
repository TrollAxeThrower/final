﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardView : MonoBehaviour
{
    [SerializeField]
    private GameObject cubePrefab;
    [SerializeField]
    public Transform boardStartingPoint;

    private Board m_Board;
    [HideInInspector]
    public GameObject[,,] BoardMadeOfPrefabs =
    new GameObject[Board.matDimX, Board.matDimY, Board.matDimZ];
    private GameObject m_CubesParent;

    public Board CreateBoard()
    {
        Vector3 pos = boardStartingPoint.position;
        m_Board = new Board();
        m_Board.ResetBoard();

        if (m_CubesParent != null)
            DestroyImmediate(m_CubesParent);

        m_CubesParent = new GameObject("Cubes Parent");
        m_CubesParent.transform.parent = this.transform;

        for (int x = 0; x < Board.matDimX; x++)
        {
            for (int z = 0; z < Board.matDimZ; z++)
            {
                for (int y = 0; y < Board.matDimY; y++)
                {
                    GameObject cube =
                    Instantiate
                        (cubePrefab,
                        new Vector3(x + pos.x, y + pos.y, z + pos.z),
                        Quaternion.identity,
                        this.transform);
                    cube.name =
                        x.ToString() + " " +
                        y.ToString() + " " +
                        z.ToString();
                    BoardMadeOfPrefabs[x, y, z] = cube;
                    cube.transform.parent = m_CubesParent.transform;
                    cube.SetActive(false);
                }
            }
        }
        Debug.Log("Board Creation Completed");

        return m_Board;
    }

    public void UpdateBoardViewAccordingToBoard()
    {
        m_Board.DoActionForEachCubeInMatrix(ShowCube, HideCube);
    }

    private void ShowCube(int x, int y, int z)
    {
        BoardMadeOfPrefabs[x, y, z].SetActive(true);
    }

    private void HideCube(int x, int y, int z)
    {
        BoardMadeOfPrefabs[x, y, z].SetActive(false);
    }
}